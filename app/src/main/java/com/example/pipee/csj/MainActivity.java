package com.example.pipee.csj;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edMail, edPass;
    private Button btnLogin;
    private TextView txtRegistrar;
FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

this.edMail = (EditText) findViewById(R.id.edMail);
this.edPass = (EditText)findViewById(R.id.edPass);
this.btnLogin = (Button) findViewById(R.id.btnLogin);
this.txtRegistrar = (TextView) findViewById(R.id.txtRegistrar);


btnLogin.setOnClickListener(this);
txtRegistrar.setOnClickListener(this);

mAuthListener = new FirebaseAuth.AuthStateListener() {
    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null){

            Log.i("SESION", "Sesion iniciadad con el email: "+ user.getEmail());

        }else {
            Log.i("Sesion","Sesion cerrada");
        }

    }
};

}

    public void Login(String email, String pass){
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email,pass);
    }
    public void Registrar(String email, String pass){
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    Log.i("SESION","Usuario creado correctamente");
                }else{
                    Log.e("SESION", task.getException().getMessage()+"");
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnLogin:

                String emailIni = edMail.getText().toString();
                String passIni = edPass.getText().toString();
                Login(emailIni,passIni);
                break;

            case R.id.txtRegistrar:

                 String emailReg = edMail.getText().toString();
                 String passReg = edPass.getText().toString();
                 Registrar(emailReg,passReg);
                 break;

        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseAuth.getInstance().addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null){
            FirebaseAuth.getInstance().removeAuthStateListener(mAuthListener);
        }

    }
}
